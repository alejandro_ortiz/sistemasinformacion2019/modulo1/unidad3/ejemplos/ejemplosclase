﻿DROP DATABASE IF EXISTS programacion;
CREATE DATABASE programacion;
USE programacion;

/*
  Ejemplos de procedimientos almacenados
*/

-- p1. Crear un procedimiento almacenado que muestre la fecha de hoy


  DELIMITER //
  CREATE OR REPLACE PROCEDURE p1()
    BEGIN
      SELECT NOW();
    END //
  DELIMITER ;

  -- llamada al prodecimiento p1
  CALL p1();

-- p2. Crear una variable de tipo datetime y almacenar en esa varibale la fecha de hoy. 
-- Nos muestre la variable

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p2()
    BEGIN
      -- declarar
      DECLARE v datetime;
      -- input (inicializar)
      SET v = NOW();
      -- output
      SELECT v;
    END //
  DELIMITER ;
  
  -- llamada al prodecimiento p2
  CALL p2();

-- p3. Suma 2 numeros
-- Recibe como argumento dos numeros y me muestra la suma

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p3(numero1 int,numero2 int)
    BEGIN
      DECLARE suma int DEFAULT 0;
      SET suma = numero1 + numero2;
      SELECT suma;
    END //
  DELIMITER ;

  CALL p3(1,5);
  CALL p3(88,2);

-- p4. Resta 2 numeros
-- Recibe como argumento dos numeros y me muestra la resta

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p4(numero1 int,numero2 int)
    BEGIN
      DECLARE resultado int DEFAULT 0;
      SET resultado = numero1 - numero2;
      SELECT resultado;
    END //
  DELIMITER ;

  CALL p4(1,5);
  CALL p4(88,2);

-- p5. Crear un procedimiento que sume dos numeros y los 
-- almacena en una tabla llamada datos.
-- La tabla la debe crear en caso de que no existe.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p5(num1 int,num2 int)
    BEGIN
      -- Declarar e inicializar las variables
      DECLARE resultado int DEFAULT 0;
      
      -- Creamos la tabla
      CREATE TABLE IF NOT EXISTS datos(
        id int AUTO_INCREMENT,
        numero1 int DEFAULT 0,
        numero2 int DEFAULT 0,
        suma int DEFAULT 0,
        PRIMARY KEY (id)
      );

      -- Realizar los calculos 
      SET resultado = num1 + num2;

      -- Almacenar el resultado en la tabla
      INSERT INTO datos VALUES (DEFAULT, num1, num2, resultado);
      
    END //
  DELIMITER ;

  CALL p5(14,5);
  CALL p5(1,56);

  SELECT * FROM datos d;

-- p6. Crear un procedimiento que calcula la suma y el producto de dos numeros y los 
-- almacena en una tabla llamada sumaproducto(id,d1,d2,suma,producto).
-- La tabla la debe crear en caso de que no existe.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p6(num1 int, num2 int)
    BEGIN
      -- Declarar e inicializar las variables
      DECLARE vsuma int DEFAULT 0;
      DECLARE vproducto int DEFAULT 0;

      -- Realizar los calculos
      SET vsuma = num1 + num2;
      SET vproducto = num1 * num2;

      -- Creamos la tabla
      CREATE TABLE IF NOT EXISTS sumaproducto(
        id int AUTO_INCREMENT,
        numero1 int DEFAULT 0,
        numero2 int DEFAULT 0,
        suma int DEFAULT 0,
        producto int DEFAULT 0,
        PRIMARY KEY (id)
      );
      
      -- Almacenar el resultado en la tabla
      INSERT INTO sumaproducto VALUES (DEFAULT, num1, num2, vsuma, vproducto);

    END //
  DELIMITER ;

  CALL p6(14,5);
  CALL p6(1,56);
  CALL p6(4,5);

  SELECT * FROM sumaproducto s;

-- p7. Crear un procedimiento que eleva un numero a otro. Almacenar el resultado
-- y los numeros en la tabla potencia. se le pasan dos numeros, base y exponente.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p7(base int, exponente int)
    BEGIN
      -- Declarar e inicializar variables
      DECLARE vpotencia int DEFAULT 0;

      -- Realizar los calculos
      SET vpotencia = POW(base, exponente);
    
      -- Creamos la tabla 
      CREATE TABLE IF NOT EXISTS potencia(
        id int AUTO_INCREMENT,
        numerob int DEFAULT 0,
        numeroe int DEFAULT 0,
        potencia int DEFAULT 0,
        PRIMARY KEY (id)
      );

      -- Almacenar el resultado en la tabla
      INSERT INTO potencia (numerob, numeroe, potencia) VALUES (base, exponente, vpotencia);
         
    END //
  DELIMITER ;

  CALL p7(2,3);
  CALL p7(4,4);
  CALL p7(5,3);

  SELECT * FROM potencia p;

-- p8. Crear un procedimiento que calcule la raiz cuadrada. Almacenar el numero 
-- y el resultado en la tabla raiz.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p8(num int)
    BEGIN
      
      DECLARE vraiz float DEFAULT 0;

      SET vraiz = SQRT(num);

      CREATE TABLE IF NOT EXISTS raiz(
        id int AUTO_INCREMENT,
        numero int DEFAULT 0,
        raiz float DEFAULT 0,
        PRIMARY KEY (id)
      );

      INSERT INTO raiz (numero, raiz) VALUES (num, vraiz);

    END //
  DELIMITER ;

  CALL p8(9);
  CALL p8(15);

  SELECT * FROM raiz r;

-- p9. Analizar el siguiente procedimento y añadir lo necesario para
-- que calcule la longitud del texto y lo almacene en la tabla en un 
-- campo llamado longitud.
  
  DELIMITER //
  CREATE OR REPLACE PROCEDURE p9(argumento varchar(50))
    BEGIN
      DECLARE vlongitud int DEFAULT 0;

      SET vlongitud = CHAR_LENGTH(argumento);

      CREATE TABLE IF NOT EXISTS texto(
        id int AUTO_INCREMENT PRIMARY KEY,
        texto varchar(50),
        longitud int
      );
    
      INSERT INTO texto(texto, longitud) VALUE (argumento, vlongitud);
    END //
  DELIMITER ;

  CALL p9('casa');
  CALL p9('racing santander');
  
  SELECT * FROM texto t;

  /**
    CONTROL DE FLUJO, INSTRUCCIONES DE SELECCION
  **/

  /**
    sintaxis basica del if
    
      IF (condicion) THEN
        sentencias verdadero;
      ELSE
        sentencias falso;
      END IF;

  **/

-- p10. Procedimiento al cual le paso un numero y me indica si es mayor que 100

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p10(num int)
    BEGIN
      
      IF (num>100) THEN
        SELECT 'mayor que 100';
      END IF;

    END //
  DELIMITER ;
  
  CALL p10(150);
  CALL p10(25);

-- p11. Procedimiento al cual le paso un numero y me indica si es mayor, igual
-- o menor que 100

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p11(num int)
    BEGIN
      IF (num>100) THEN
        SELECT 'Es mayor que 100';
      ELSEIF (num<100) THEN
        SELECT 'Es menor que 100';
      ELSE
        SELECT 'Es igual a 100';

      END IF;
    END //
  DELIMITER ;

  CALL p11(175);
  CALL p11(75);
  CALL p11(100);

  -- mejor programado
  DELIMITER //
  CREATE OR REPLACE PROCEDURE p11b(num int)
    BEGIN

      DECLARE vresultado varchar(50) DEFAULT 'Es igual a 100';
      
      IF (num>100) THEN
        SET vresultado = 'Es mayor que 100';
      ELSEIF (num<100) THEN
        SET vresultado = 'Es menor que 100';
      END IF;

      SELECT vresultado;
    END //
  DELIMITER ;

  CALL p11b(175);
  CALL p11b(75);
  CALL p11b(100);

-- p12. Crear un procedimiento que le pases una nota y te debe devolver lo siguiente
-- Suspenso: si la nota es menor que 5
-- Suficiente: si la nota esta entre 5 y 7
-- Notable: si la nota esta entre 7 y 9
-- Sobresaliente: Si la nota es mayor que 9 

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p12(nota float)
    BEGIN
      
      DECLARE vresultado varchar (50) DEFAULT 'Suspenso';

      IF (nota >= 5 AND nota < 7) THEN
        SET vresultado = 'Suficiente';
      ELSEIF (nota >= 7 AND nota < 9) THEN
        SET vresultado = 'Notable';
      ELSEIF (nota >= 9) THEN
        SET vresultado = 'Sobresaliente';
      END IF;

      SELECT vresultado;

    END //
  DELIMITER ;

  CALL p12(3.4);
  CALL p12(5);
  CALL p12(6.4);
  CALL p12(7);
  CALL p12(8.1);
  CALL p12(9);
  CALL p12(9.5);

-- p13. El mismo ejercicio que el anterior pero con CASE

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p13(nota float)
    BEGIN
      
      DECLARE vresultado varchar (50);

      CASE 
        WHEN (nota >= 5 AND nota < 7) THEN
          SET vresultado = 'Suficiente';
        WHEN (nota >= 7 AND nota < 9) THEN
          SET vresultado = 'Notable';
        WHEN (nota >= 9) THEN
          SET vresultado = 'Sobresaliente';
        ELSE 
          SET vresultado = 'Suspenso';
      END CASE;

      SELECT vresultado;

    END //
  DELIMITER ;

  CALL p13(3.4);
  CALL p13(5);
  CALL p13(6.4);
  CALL p13(7);
  CALL p13(8.1);
  CALL p13(9);
  CALL p13(9.5);

-- ARGUMENTOS DE ENTRADA/SALIDA

-- p14. Quiero calcular la suma de dos numeros.
-- La suma la almacena en un argumento de salida llamado resultado
-- CALL p14(num1 int, num2 int, OUT resultado int)

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p14(num1 int, num2 int, OUT resultado int)
    BEGIN
      SET resultado  = num1 + num2;
    END //
  DELIMITER ;

  SET @suma = 0;
  CALL p14(2,5,@suma);
  SELECT @suma;

-- p15. Procedimiento que recibe como argumentos:
-- num1: numero (argumento de entrada)
-- num2: numero (argumento de entrada)
-- resultado: numero (argumento de salida o entrada/salida)
-- calculo: texto (argumento de entrada)
-- si calculo vale suma entonces resultado tendra la suma de num1+num2
-- si calculo vale producto entonces resultado tendra el producto de num1*num2

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p15(num1 int, num2 int, OUT resultado int, calculo varchar(10))
    BEGIN
      IF (calculo = 'suma') THEN
        SET resultado = num1 + num2; 
      ELSEIF (calculo = 'producto') THEN
        SET resultado = num1 * num2;
      END IF;
    END //
  DELIMITER ;

  CALL p15(7,2,@resultado,'suma');
  SELECT @resultado;

  CALL p15(7,2,@resultado,'producto');
  SELECT @resultado;

/**
  PROCEDIMIENTOS CON BUCLES
**/

-- p16. Utilizando while mostrar por pantalla numeros del 1 al 10

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p16()
    BEGIN
    
      -- Creamos vaiable
      DECLARE contador int DEFAULT 1;
  
      -- creamos el bucle
      WHILE (contador<11) DO
        -- mostramos la variable
        SELECT contador;
  
        -- incrementamos la variable
        SET contador = contador + 1;
      END WHILE;

    END //
  DELIMITER ;

CALL p16();

-- p17. Modificar el anterior para que se introduzcan los datos en una 
-- tabla temporal y lo muestre

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p17()
    BEGIN
    
      -- Creamos vaiable
      DECLARE contador int DEFAULT 1;

      -- Creamos una tabla temporal
      CREATE OR REPLACE TEMPORARY TABLE numeros(
        id int AUTO_INCREMENT,
        numero int,
        PRIMARY KEY (id)
      );
  
      -- creamos el bucle
      WHILE (contador<11) DO
        -- insertamos el dato en la tabla
        INSERT INTO numeros (numero) VALUES (contador);
  
        -- incrementamos la variable
        SET contador = contador + 1;
      END WHILE;

      -- mostramos la tabla
      SELECT * FROM numeros;

    END //
  DELIMITER ;

  CALL p17();

-- p18. Crea un procedimiento que le pasas un numero y te muestra 
-- desde el numero 1 hasta el ese numero. Utilizar tabla temporal y while

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p18(numero int)
    BEGIN
      
      -- Creamos vaiable
      DECLARE contador int DEFAULT 1;

      -- Creamos una tabla temporal
      CREATE OR REPLACE TEMPORARY TABLE numeros( 
        dato int
      );
  
      -- creamos el bucle
      WHILE (contador <= numero) DO
        -- insertamos el dato en la tabla
        INSERT INTO numeros (dato) VALUES (contador);
  
        -- incrementamos la variable
        SET contador = contador + 1;
      END WHILE;

      -- mostramos la tabla
      SELECT * FROM numeros;
      
    END //
  DELIMITER ;

  CALL p18(15);
  CALL p18(200);