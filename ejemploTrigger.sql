﻿DROP DATABASE IF EXISTS ejemploTrigger;
CREATE DATABASE ejemploTrigger;
USE ejemploTrigger;

  CREATE OR REPLACE TABLE local (
    id int AUTO_INCREMENT,
    nombre varchar(50),
    precio float,
    plazas int,
    PRIMARY KEY (id)
  );

  CREATE OR REPLACE TABLE usuario (
    id int AUTO_INCREMENT,
    nombre varchar(50),
    inicial char(1),
    PRIMARY KEY (id)
  );

  CREATE OR REPLACE TABLE usa (
    local int,
    usario int,
    total float,
    fecha date,
    dias int,
    PRIMARY KEY (local,usario)
  );
  /**
  ALTER TABLE usa
    ADD CONSTRAINT fkUsaLocal FOREIGN KEY (local) REFERENCES local (id),
    ADD CONSTRAINT fkUsaUsuario FOREIGN KEY (usario) REFERENCES local (id);
  */

-- 1. Quiero que cuando introduzca un usuario me calcule y almacene la inicia de su nombre.

  DELIMITER //
  CREATE OR REPLACE TRIGGER usuarioBI
    BEFORE INSERT
    ON usuario
    FOR EACH ROW
    BEGIN
      SET NEW.inicial = LEFT(NEW.nombre,1);
    END //
  DELIMITER ;

  TRUNCATE usuario;
  INSERT INTO usuario (nombre)
    VALUES ('Roberto'),('ana'),('kevin');
  SELECT * FROM usuario u;

  /* modificamos la tabla usuario */

  ALTER TABLE usuario
    ADD COLUMN contador int DEFAULT 0;

-- 2. Inserto un usuario en usa me debe sumar a contador 1

  DELIMITER //
  CREATE OR REPLACE TRIGGER usaAI
    AFTER INSERT
    ON usa
    FOR EACH ROW
    BEGIN
      UPDATE usuario u 
        SET u.contador = u.contador + 1
        WHERE u.id = NEW.usario;
    END //
  DELIMITER ;

  TRUNCATE usa;
  INSERT INTO usa (local, usario)
    VALUES (1, 1),(1, 2),(2, 1); 

  SELECT * FROM usa u;
  SELECT * FROM usuario u;
  
-- 3. Consulta de actualizacion que realice lo que hace el disparador anterior
-- Me tiene que colocar en contador las veces que ese usuario aparece en usa
  
  SELECT u.usario, COUNT(*)numVeces FROM usa u GROUP BY u.usario;

  UPDATE usuario u 
    JOIN (
      SELECT u.usario, COUNT(*)numVeces 
        FROM usa u 
          GROUP BY u.usario)c1
    ON c1.usario = u.id
      SET u.contador = c1.numVeces;

  -- otra forma
  UPDATE usuario u 
    SET contador = ( 
      SELECT COUNT(*)numVeces 
        FROM usa us 
          WHERE us.usario = u.id);

-- 4. Disparador que calcule el total al insertar un usuario   
  
  DELIMITER //
  CREATE OR REPLACE TRIGGER usaBI
    BEFORE INSERT
    ON usa
    FOR EACH ROW
    BEGIN
      SET new.total = new.dias * (
        SELECT l.precio FROM local l WHERE l.id = new.local);
    END //
  DELIMITER ; 
  
  TRUNCATE usa;
  INSERT INTO local (nombre, precio)  
    VALUES ('l1', 10),('l2', 5),('l3', 7);

  INSERT INTO usa (local, usario, dias)
    VALUES (1, 1, 5), (2, 1, 8);

  SELECT * FROM usa u;
  SELECT * FROM local l;