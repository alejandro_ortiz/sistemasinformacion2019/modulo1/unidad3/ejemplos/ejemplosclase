﻿USE programacion;

  CREATE OR REPLACE TABLE salas(
    id int AUTO_INCREMENT PRIMARY KEY,
    butacas int,
    fecha date,
    edad int DEFAULT 0
  );

  CREATE OR REPLACE TABLE ventas(
    id int AUTO_INCREMENT PRIMARY KEY,
    sala int,
    numero int DEFAULT 0
  );

-- salasBI:
-- Controla la insercion de registros nuevos
-- Disparador para que me calcule la edad de la sala en funcion de su fecha de alta

  DELIMITER //
  CREATE OR REPLACE TRIGGER salasBI
    BEFORE INSERT
    ON salas
    FOR EACH ROW
    BEGIN
      SET NEW.edad = TIMESTAMPDIFF(year,NEW.fecha,NOW());
      /**
      UPDATE salas 
        SET edad=TIMESTAMPDIFF(year,fecha,NOW());
      */
    END //
  DELIMITER ;

  -- Inserto un registro en la tabla para probar el disparador
  INSERT INTO salas (butacas, fecha)
    VALUES (50, '2010/01/01'), (50, '2000/01/01');
  SELECT * FROM salas s;

-- salasBU:
-- Controla la actualizacion de registros
-- Disparador para que me calcule la edad de la sala en funcion de su fecha de alta

  DELIMITER //
  CREATE OR REPLACE TRIGGER salasBU
    BEFORE UPDATE 
    ON salas
    FOR EACH ROW
    BEGIN
      SET NEW.edad = TIMESTAMPDIFF(year,NEW.fecha,NOW());
    END //
  DELIMITER ;

  -- Actualizo un dato de la tabla pra probar el disparador
  UPDATE salas 
    SET fecha = '2002/01/01' 
    WHERE id=2;  
  SELECT * FROM salas s;

  -- Comprobar funcionamieto de ambos disparadores
  INSERT salas (butacas, fecha)
    VALUES (56, '2005/4/8'),(23, '1989/5/5');
  SELECT * FROM salas s;

  UPDATE salas
    SET fecha='1990/1/1'; 
  SELECT * FROM salas s;

-- Necesito que la tabla salas disponga de tres campos nuevos.
-- Esos campos seran dia, mes, año.
-- Quiero que esos tres campos AUTOMATICAMENTE tengan el dia, mes y año de la fecha.
-- Necesito un disparadador para insertar y otro para actualizar.

  ALTER TABLE salas
    ADD COLUMN dia int,
    ADD COLUMN mes int,
    ADD COLUMN anio int;

  DELIMITER //
  CREATE OR REPLACE TRIGGER salasBI
    BEFORE INSERT
    ON salas
    FOR EACH ROW
    BEGIN
      SET NEW.edad = TIMESTAMPDIFF(year,NEW.fecha,NOW());

      SET NEW.dia = DAY(NEW.fecha);
      SET NEW.mes = MONTH(NEW.fecha);
      SET NEW.anio = YEAR(NEW.fecha);
    END //
  DELIMITER ;

  DELIMITER //
  CREATE OR REPLACE TRIGGER salasBU
    BEFORE UPDATE 
    ON salas
    FOR EACH ROW
    BEGIN
      SET NEW.edad = TIMESTAMPDIFF(year,NEW.fecha,NOW());

      SET NEW.dia = DAY(NEW.fecha);
      SET NEW.mes = MONTH(NEW.fecha);
      SET NEW.anio = YEAR(NEW.fecha);
    END //
  DELIMITER ;

  -- comprobar el funcionamiento

  INSERT salas (butacas, fecha)
    VALUES (78, '2005/4/8'),(120, '1989/5/5');
  SELECT * FROM salas s;

  UPDATE salas 
    SET edad = 10;
  SELECT * FROM salas s;

-- Vamos a realizar un ejercicio que nos permita actualizar correctamente los datos en una nueva tabla.
-- Ventas1 (id, fecha, precioUnitario, unidades, precioFinal)
-- Debe ser capaz de calcular el precioFinal con disparadores
-- PrecioFinal = Unidades*precioUnitario
-- Crear un disparador para insertar y otro para actualizar

  CREATE OR REPLACE TABLE ventas1(
    id int AUTO_INCREMENT PRIMARY KEY,
    fecha date,
    preciounitario int,
    unidades int,
    preciofinal int
  );

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventas1BI
    BEFORE INSERT
    ON ventas1
    FOR EACH ROW
    BEGIN
      SET NEW.preciofinal = NEW.preciounitario * NEW.unidades;
    END //
  DELIMITER ;

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventas1BU
    BEFORE UPDATE
    ON ventas1
    FOR EACH ROW
    BEGIN
      SET NEW.preciofinal = NEW.preciounitario * NEW.unidades;
    END //
  DELIMITER ;

  INSERT INTO ventas1 (fecha, preciounitario, unidades)
    VALUES ('2000/1/1', 34, 5),('2010/2/3',10,7);
  SELECT * FROM ventas1;

  UPDATE ventas1 
    SET preciounitario = 12 
    WHERE id = 2;
  SELECT * FROM ventas1;