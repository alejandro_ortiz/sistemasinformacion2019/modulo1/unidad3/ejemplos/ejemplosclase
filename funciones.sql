﻿USE programacion;

-- f1. Le paso dos numeros y me devuelve la suma de ellos

  DELIMITER //
  CREATE OR REPLACE FUNCTION f1(numero1 int, numero2 int)
    RETURNS int
    BEGIN
      DECLARE vsuma int DEFAULT 0;

      SET vsuma = numero1 + numero2;

      RETURN vsuma;
    END //
  DELIMITER ;

  SELECT f1(5,6);